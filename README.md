# public-doc

Technical documentation repo for OpenCraft

See [doc.opencraft.com](https://doc.opencraft.com/) for the latest published version (hosted by GitLab Pages).

To build the docs to HTML and view them locally, run from the root repository
directory:

```bash
pip install -r requirements.txt
mkdocs serve
```
