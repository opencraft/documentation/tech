# Setting up your devstacks

At OpenCraft, we use many versions of the OpenEdX platform and correspondingly we need to have multiple 'devstacks' on our computers. A devstack is a fully functional version of OpenEdX optimized for development.

There is the 'master devstack' which mirrors the master branches of all the OpenEdX repositories, and then there are devstacks mapped to named releases of OpenEdX (Such as Koa, Juniper, Ironwood, etc). Setting up your devstack can be time-consuming, and if you make a mistake even more so. This guide will help you set up all your devstacks.

Installing direnv
--

Before you start you should set up direnv, this will help you use more than one devstacks without interference.

[For instructions on installing direnv, click here.](https://direnv.net/docs/installation.html)

Why are we doing this? Because we'd like to be able to set a very important environment variable on a per-directory basis: COMPOSE_PROJECT_NAME. It is vital to set this environment variable before installing your devstack because leaving it in the default state will mean you will not be able to install the ironwood release of the devstack.

Setting up your master devstack
--

To set up your master devstack, first clone the devstack repository provided by edx:

`git clone https://github.com/edx/devstack.git`

`cd devstack`

Now create a file .envrc with your favorite text editor and the following contents:

`export COMPOSE_PROJECT_NAME=master_devstack`

Now activate the .envrc file with

`direnv allow`

You can replace the word 'master_devstack' with any other unique name you'd like to give to this devstack. **Do not use just 'devstack' as the value**.

As the master devstack is constantly being updated, you can find the next steps here: https://github.com/edx/devstack#getting-started


Setting up a named release devstack (Koa, Juniper, etc)
--

The steps to install a devstack for a named release is similar to setting up the master devstack with a few extra steps

To set up your named devstack, first clone the devstack repository provided by edx:

`git clone https://github.com/edx/devstack.git`

`cd devstack`

Now check out the named release branch of the devstack. For example:

`git checkout open-release/koa.master`

Now create a file .envrc with your favorite text editor and the following contents:
```
export OPENEDX_RELEASE=koa.master
export COMPOSE_PROJECT_NAME=koa_devstack
```

You can also specify particular tagged releases in OPENEDX_RELEASE for example: `open-release/koa.2` `open-release/juniper.1` etc. Note that you do not need to checkout the tagged release via git checkout within the devstack.

Now activate the .envrc file with

`direnv allow`

You can replace the word 'koa_devstack' with any other unique name you'd like to give to this devstack. **Do not use 'devstack'**.

The rest of the instructions are the same as the master devstack.

Setting up an ironwood devstack
--

Ironwood was the first release on the new Docker-based devstacks and it has a few wrinkles in setting up. Since ironwood related work is rare, you can set aside this step until you have an ironwood related task assigned.

To start:

`git clone https://github.com/edx/devstack.git`

`cd devstack`

`git checkout open-release/ironwood.master`

Now create a file .envrc with your favorite text editor and the following contents:
```
export OPENEDX_RELEASE=ironwood.master
```

Notice we don't set COMPOSE_PROJECT_NAME here. This is because the ironwood devstack has this variable hardcoded to 'devstack' (Hence we asked you not to use it anywhere else earlier in the tutorial). Unfortunately, this also means you can't have two ironwood devstacks on the same computer.


Now activate the .envrc file with

`direnv allow`

The rest of the instructions are the same as the master devstack

Environment variables explained
--

In the above steps we setup two environment variables using direnv during setup, here's why they matter:

* `OPENEDX_RELEASE`: Open EdX releases various tagged releases with all the participating OpenEdX releases. Think of it as a 'snapshot' of the accepted Git history at a particular point, or a coordinated point in all the various OpenEdX repositories that have been tested to work together.
* `COMPOSE_PROJECT_NAME`: When creating docker images, this variable is used to give a unique name to your docker images (So that two devstacks do not interfere with eachother, even if they use the same release). Unfortunately this doesn't work for the ironwood devstack.