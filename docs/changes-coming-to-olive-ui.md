# Changes Coming to  Open edX UI

The Open edX ecosystem has slowly but surely been working on a full revamp of
its user interface, mainly to move towards a newer architecture, and completely
rewritten using newer technologies built around React.

Since this is an enormous process, bits and pieces of old UI are being replaced
with new react-based versions. Till now, it is still possible to
continue using the older UI. However, starting with the Olive release,
large parts of the UI are no longer optional.

Most importantly, the courseware UI that forms most of the learning
experience has been rebuilt from scratch.

These newer UIs use a completely different mechanism for theming.
It is possible to apply minimal theming changes without creating
a full fork, which would require additional maintenance.

We are working with the relevant parties (Axim, 2U, and other providers) on a
better system for theming that will allow more extensive changes without
additional maintenance.

We will need to expend some effort into converting existing comprehensive themes
into a format that will work with the existing parts of the UI and the new ones.

That said, here are some of the changes you can expect with the Olive release:

## Courseware UI changes

The courseware UI is now its own hosted single page app that loads content dynamically. The visual changes don't convey it, but it is a complete rewrite.

| ![Courseware UI in Nutmeg](./images/olive-changes/courseware/courseware-nutmeg.png) |
| :-: |
| **The old courseware UI in Nutmeg** |

| ![Courseware UI in Olive](./images/olive-changes/courseware/olive-courseware-mfe.png) |
| :-: |
| **The new courseware UI in Olive** |

| ![Coursware UI in Olive has a guided tour](./images/olive-changes/courseware/olive-courseware-mfe-tour.png) |
| :-: |
| **The new courseware UI [comes with a guided tour](https://openedx.org/blog/nutmeg-release-highlight-blog-series-product-tours/)** |



## The New Accounts UI

The user account settings UI has also been replaced with a newer version.

| ![Account settings page in Nutmeg](./images/olive-changes/accounts/nutmeg-account-settings.png) |
| :-: |
| **The old account settings page in Nutmeg.** |

| ![The account settings page in Olive](./images/olive-changes/accounts/olive-account-settings-mfe.png) |
| :-: |
| **The account settings page in Olive** |


## The new user profile UI

The user profile UI has also been revamped.

| ![Nutmeg profile editor](./images/olive-changes/profile/nutmeg-profile-editor.png) |
| :-: |
| **The old user profile page** |

| ![Olive profile editor](./images/olive-changes/profile/olive-profile-editor-mfe.png) |
| :-: |
| **The new user profile editor in Olive** |

# Additional Optional Changes

There are yet more upcoming changes that can be previewed that are not required at this stage but might provide useful features that make them worth opting into.

## The new Gradebook UI

The new Gradebook UI provides an improved user grade viewing and editing experience. A detailed overview of the gradebook can be found [here](https://opencraft.com/a-new-editable-gradebook-for-open-edx/).

| ![Old gradebook viewer](./images/olive-changes/gradebook/nutmeg-gradebook.png) |
| :-: |
| **The older Gradebook UI in the Instructor Dashboard** |

| ![New gradebook viewer](./images/olive-changes/gradebook/olive-gradebook-mfe.png) |
| :-: |
| **The new Gradebook viewer** |

| ![New gradebook editor](./images/olive-changes/gradebook/olive-gradebook-mfe-edit-grades.png) |
| :-: |
| **The new Gradebook editor** |

## The new Login and Registration UI

The new login and registration UI is also available. Currently this UI contains some hard-coded references to edX which require some extra effort to remove. This should be fixed in future releases.

| ![Old login UI](./images/olive-changes/logistration/nutmeg-login.png) |
| :-: |
| **Old login UI** |

| ![New login UI](./images/olive-changes/logistration/olive-login.png) |
| :-: |
| **New login UI with username and email option** |

| ![Old registration UI](./images/olive-changes/logistration/nutmeg-register.png) |
| :-: |
| **Old registration UI** |

| ![New registration UI](./images/olive-changes/logistration/olive-register.png) |
| :-: |
| **New registration UI** |

An optional mode allows splitting the registration into two steps, and collecting additional data in a secondary page:

| ![New progressive profiling UI](./images/olive-changes/logistration/olive-register-progressive.png) |
| :-: |
| **New registration UI** |

## The new Course Authoring UI

The new Course Authoring MFE adds the UI to configure parts of the course experience that were only available via advanced course settings. It doesn't completely replace existing UI but provides a more straightforward UI for previously hidden settings.

| ![The new course authoring UI](./images/olive-changes/course-authoring/course-authoring-mfe.png) |
| :-: |
| **The new Course Authoring MFE allows configuring the course progress page, the wiki, the calculator tool, notes, and discussions, exposing settings previously tucked away in advanced settings.** |

| ![Enabling/disabling notes ](./images/olive-changes/course-authoring/course-authoring-mfe-notes-config.png) |
| :-: |
| **Enable or disable the notes tool with a single click.** |

| ![Select a discussion provider](./images/olive-changes/course-authoring/course-authoring-mfe-discussions-select.png) |
| :-: |
| **Select a discussion provider.** |

| ![Configuring discussions](./images/olive-changes/course-authoring/course-authoring-mfe-discussions-config.png) |
| :-: |
| **Most relevant discussions configuration settings are now available in one place in the course authoring MFE.** |

| ![New text editor](./images/olive-changes/course-authoring/course-authoring-mfe-new-text-editor.png) |
| :-: |
| **A new editor for text and html is also available in this MFE. Once enabled, edits to text/html components will redirect to this new editor page.** |



## The new Discussions UI

The discussions/forum experience in Open edX is being revamped with a new UI and new features. More information about this new UI is available [here](https://opencraft.com/blog/a-look-at-the-recent-enhancements-to-discussions-in-open-edx/#how-to-try-out-these-new-features), and some of its [caveats are noted in the Olive release notes](https://docs.openedx.org/en/latest/community/release_notes/olive.html#discussions-micro-frontend-mfe)

| ![The new discussions UI](./images/olive-changes/discussions/olive-discussions-mfe.png) |
| :-: |
| **The new discussions experience refreshes the UI and adds new features as well.** |
