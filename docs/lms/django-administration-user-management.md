# Django Administration: User Management

## User Management

Your account has been updated to grant you "superuser" access, which gives you edit rights in the Administration area of your Open edX site. 

If you're currently logged in, you may have to logout and back in again once to make your superuser setting take effect.

Your Django Administration site is located at **https://your-lms-url.com/admin/**

This site is a web front-end to your edxapp mysql database, and so there's links for almost everything. It will let you do bad things too, like deleting whole swaths of users, or changing really basic settings that make your site unusable. So use with care.

## How to Login

Login using your username and password. This is different from the main Open edX LMS site, where you can login with your email address.

![](../images/lms-django-usermanagement-login.png)

## How to Find a User

Scroll down to the "Authentication and Authorization" section to find the Users link circled below.

![](../images/lms-django-usermanagement-users-section.png)

From that screen, you can see a list of all the Users who have ever had accounts on your LMS. The easiest way to find someone, is to search, e.g.:

![](../images/lms-django-usermanagement-search-user.png)

## How to Activate or Elevate a User's Privileges

Click on a Username to see the Edit screen for that user. From there, you can see the **Permissions** section, with checkboxes for **Active**, **Staff status** and **Superuser status**.

![](../images/lms-django-usermanagement-activate-user.png)

- To activate a user: check the **Active** checkbox.
- To grant staff access to all your courses: check the **Staff status** checkbox.
- To grant access to this Django Administration panel: check the **Superuser status** checkbox.

Click Save at the bottom of the page to save your changes.

## How to Reset a Password

**Note**: This feature sits behind [a feature flag](https://edx.readthedocs.io/projects/edx-platform-technical/en/latest/featuretoggles.html#featuretoggle-FEATURES['ENABLE_CHANGE_USER_PASSWORD_ADMIN']), which you have to enable in order to see the password reset form in Django administration view. It is disabled by default.


Click on a Username to see the Edit screen for that user. From there, you can see a link to a form to change a user's password:

![](../images/lms-django-usermanagement-change-password-form-position.png)

The password reset form looks like this:

![](../images/lms-django-usermanagement-change-password-form-ui.png)
