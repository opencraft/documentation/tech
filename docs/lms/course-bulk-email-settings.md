# Course Bulk Email Settings

First, to send bulk emails from any of your courses without additional configuration, adjust the `Require course email auth` flag under the `Bulk email flags` setting item. Here, this has been set to false:

![](../images/lms-bulk-email-flag-update.png)

Second, if you want to leave the `Require course email auth` setting as is, you would need to create a new `Course Authorization` object for each course you want to send bulk emails from:

![](../images/lms-bulk-email-course-auth.png)

Both of these settings combinations are valid options that will allow you to send bulk emails from the instructor admin panes of the relevant courses.
