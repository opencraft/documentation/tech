# Open edX Language Support

Open edX Nutmeg installations support [thirty differenct languages by default](https://github.com/edx/edx-platform/blob/open-release/nutmeg.master/conf/locale/config.yaml). Aside from English (en), these include Arabic (ar), Esperanto (eo), Spanish (es-419), French (fr), Hebrew (he), Hindi (hi), Korean (ko-kr), Portuguese (pt-br), Right-to-left test language (rtl), Russian (ru), and Chinese (zh-cn). To make it possible for learners to display the platform in one of these languages, they need to be activated via the Django admin (located at: [https://yourlms.url/admin/dark_lang/darklangconfig/](https://yourlms.url/admin/dark_lang/darklangconfig/). The process for switching to a different language is as follows ([ref](https://support.edx.org/hc/en-us/articles/213195078-Can-I-change-the-language-of-the-edX-platform-)):

1. Log in to the platform.
2. Click the down arrow next to your username at the top right.
3. Select "Account" from the dropdown menu. You should now be on the Account Settings page.
4. Scroll down to the "Language" option, select the desired language from the dropdown, and press the TAB key to navigate to the next field.

    After a short delay, the page will refresh and load in the language you selected.

## Default language

The default language of an Open edX installation is controlled via the [`EDXAPP_LANGUAGE_CODE`](https://github.com/openedx/configuration/blob/open-release/nutmeg.master/playbooks/roles/edxapp/defaults/main.yml#L405) setting.

The value of this setting becomes the value of [`LANGUAGE_CODE` in the `lms.env.json` and `cms.env.json`](https://github.com/openedx/configuration/blob/open-release/nutmeg.master/playbooks/roles/edxapp/defaults/main.yml#L1411) files that are generated when provisioning a server:

## Additional languages
See the [edx-platform project on Transifex](https://www.transifex.com/open-edx/edx-platform/) for the status of translations for specific languages.

Adding support for additional languages to an Open edX installation requires a few more steps; they are described in the first section of the [Internationalization and Localization](https://github.com/edx/edx-platform/wiki/Internationalization-and-localization) wiki page.

It might be possible to install additional languages automatically during deployment (e.g. by permanently modifying `edx-platform/conf/locale/config.yaml`).

## Updating translations

The process for creating and updating translations is described in this guide (which includes information on how to contribute translations upstream): [How do I translate Open edX to another language?](https://edx.readthedocs.io/projects/edx-developer-guide/en/latest/internationalization/i18n_translators_guide.html)

Translating Open edX is a community effort, which means that you can essentially get translations (and translation updates) for free. The closer an Open edX installation is to the upstream code base, the less effort will go into creating, updating, and maintaining translations (at least for popular languages). In other words, if you have a lot of custom features, you'll need to put more effort into translations because you'll have to create/update/maintain translations for these features yourself.

## UI customizations

To make language selection even easier for users, edx-platform provides support for displaying a menu for language selection in page headers or footers, via the `EDXAPP_SHOW_HEADER_LANGUAGE_SELECTOR` and `EDXAPP_SHOW_FOOTER_LANGUAGE_SELECTOR` settings.