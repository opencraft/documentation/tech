# Devstack basics

This tutorial covers some of the basics of using the OpenEdX devstack. It is not a comprehensive guide rather it is meant to help with some frequently asked questions by newcomers.

Getting started
--

To get started with the devstack, boot it up with 

`make dev.up`

This will boot up all the services within the devstack. We can selectively boot individual service or a combination of services, this helps us to save time and get to the development faster.

For example, the most used service is LMS, to selectively start this service we can run `make dev.up.lms`, in order to run a combination of LMS and Studio we can run `make dev.up.lms+studio`.

To shut down the LMS, use `make dev.stop`. You can also use `make down` on more recent versions to both stop the devstack & remove its docker images (This helps when you have many devstacks). **Do not use `make down` with the ironwood devstack as it'll delete all your data!**

Shelling into services
--

Next, you'll want to log into a running container! You can do that with `make dev.shell.lms` (Replace 'lms' with the service you'd like to access).

Accessing logs
--

Accessing logs is as easy as `make dev.logs` or `make dev.logs.lms` for a particular service. Note that a lot of people experience the logs freezing after some time and for that reason, it might be a better idea to attach to the container instead.

Attaching to services
--

While `make dev.shell.lms` will let you shell into the same container, you can also use `make dev.attach.lms` to attach your session to the process in the container. This is helpful to view logs as `make dev.logs` can be buggy.

Compiling static files
--

When making changes to static files (Or experiencing static file related bugs) you need to compile & update static files with the command: `make static`

Note: This command takes a long time to run. If you are working on comprehensive theming you can use a shortcut by shelling into the LMS and running the following command: `paver compile_sass --system=lms --theme-dirs /edx/src/ --themes {theme_folder_name} && ./manage.py lms collectstatic --noinput`

Restarting services
--

If you want to restart a particular service without restarting the entire container (& other containers), you can use: `make lms-restart`, `make studio-restart`, etc.

More commands
--

Each release of EdX adds some variation to the commands available, and you can check them all out by reading the 'Makefile' in the devstack folder.
