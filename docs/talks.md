# Tech Talks

Here are various public talks from members of OpenCraft:

## 2022

* [__Deep dive into LTI 1.3 in the Open edX platform__](https://www.youtube.com/watch?v=Dj4UTeyJ3PM) (25 min)  
  Jill Vogel and Giovanni Cimolin da Silva | 2022-04-28
* [__So you want to become a Core Contributor?__](https://www.youtube.com/watch?v=pfiDncYRIUU) (40 min)  
  Xavier Antoviaque and Sarina Canelake (tCRIL) | 2022-04-28
* [__What is completion?__](https://www.youtube.com/watch?v=T5NP_Ie_kVw#t=8m33s) (10 min)  
  Braden MacDonald | 2022-04-28 | [Slides](https://docs.google.com/presentation/d/1_a9-9mP-aYQiNIh2jDOlRSCeYxskJu-rTaSNdYvjEvM/edit?usp=sharing)
* [__Open edX Deployments made Easy with Grove__](https://www.youtube.com/watch?v=HZ2UlDgWgpw) (20 min)  
  Kaustav Banerjee and Geoffrey Lehée | 2022-04-27
* [__UX State of Mind: Designing better Open edX themes__](https://www.youtube.com/watch?v=dUbZNQEf0_8) (30 min)  
  Cassandra Zamparini-Kern | 2022-04-27
* __How to enable the video upload pipeline__  
  Maxim Beder | 2022-04-26
* __Building your own Micro-Frontend__  
  Fox Piacenti | 2022-04-26
* __Generating reports periodically__  
  Keith Grootboom and Paulo Viadanna | 2022-04-26

## 2021

* [__Building with Blockstore - How OpenCraft Developed LabXchange Using Open edX__](https://www.youtube.com/watch?v=DARBuFcEb8w) (26 min)  
  Braden MacDonald | 2021-05-27 | [Slides](https://docs.google.com/presentation/d/1RE2AHQFbF-MsOGGjg8d652jDpc8GBt2hyoN0NGuSppk/edit)
* [__Deploying multiple Open edX instances onto a Kubernetes Cluster with Tutor__](https://www.youtube.com/watch?v=pB9huM-sgUA) (14 min)  
  Braden MacDonald | 2021-03-31 | [Discussion](https://discuss.openedx.org/t/tech-talk-demo-deploying-multiple-open-edx-instances-onto-a-kubernetes-cluster-with-tutor/4641)
